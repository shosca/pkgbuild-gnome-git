REPO=gnome-git
PWD=$(shell pwd)
DIRS=$(shell ls -d */ | sed -e 's/\///' )
ARCHNSPAWN=arch-nspawn
MKARCHROOT=/usr/bin/mkarchroot -C /usr/share/devtools/pacman-multilib.conf
PKGEXT=pkg.tar.xz
CHROOTPATH64=/var/chroot64/$(REPO)
LOCKFILE=/tmp/$(REPO)-sync.lock
PACMAN=pacman -q
REPOADD=repo-add -n --nocolor -R

TARGETS=$(addsuffix /built, $(DIRS))
PULL_TARGETS=$(addsuffix -pull, $(DIRS))
SHA_TARGETS=$(addsuffix -sha, $(DIRS))
INFO_TARGETS=$(addsuffix -info, $(DIRS))
BUILD_TARGETS=$(addsuffix -build, $(DIRS))
CHECKVER_TARGETS=$(addsuffix -checkver, $(DIRS))

.PHONY: $(DIRS) chroot

all:
	@$(MAKE) srcpull
	$(MAKE) build

clean:
	@sudo rm -rf */*.log */pkg */src */logpipe* $(CHROOTPATH64)

resetall: clean
	@sudo rm -f */built ; \
	sed --follow-symlinks -i "s/^pkgrel=[^ ]*/pkgrel=0/" $(PWD)/**/PKGBUILD ; \

chroot:
	@if [[ ! -f $(CHROOTPATH64)/root/.arch-chroot ]]; then \
		sudo mkdir -p $(CHROOTPATH64); \
		sudo rm -rf $(CHROOTPATH64)/root; \
		sudo $(MKARCHROOT) $(CHROOTPATH64)/root base-devel ; \
		sudo cp $(PWD)/pacman.conf $(CHROOTPATH64)/root/etc/pacman.conf ;\
		sudo cp $(PWD)/makepkg.conf $(CHROOTPATH64)/root/etc/makepkg.conf ;\
		sudo cp $(PWD)/locale.conf $(CHROOTPATH64)/root/etc/locale.conf ;\
		echo "MAKEFLAGS='-j$$(grep processor /proc/cpuinfo | wc -l)'" | sudo tee -a $(CHROOTPATH64)/root/etc/makepkg.conf ;\
		sudo mkdir -p $(CHROOTPATH64)/root/repo ;\
		sudo bsdtar -czf $(CHROOTPATH64)/root/repo/$(REPO).db.tar.gz -T /dev/null ; \
		sudo ln -sf $(REPO).db.tar.gz $(CHROOTPATH64)/root/repo/$(REPO).db ; \
		sudo $(ARCHNSPAWN) $(CHROOTPATH64)/root /bin/bash -c "yes | $(PACMAN) -Syu ; yes | $(PACMAN) -S gcc-multilib gcc-libs-multilib p7zip && chmod 777 /tmp" ; \
		echo "builduser ALL = NOPASSWD: /usr/bin/pacman" | sudo tee -a $(CHROOTPATH64)/root/etc/sudoers.d/builduser ; \
		echo "builduser:x:$${SUDO_UID:-$$UID}:100:builduser:/:/usr/bin/nologin\n" | sudo tee -a $(CHROOTPATH64)/root/etc/passwd ; \
		sudo mkdir -p $(CHROOTPATH64)/root/build; \
	fi ; \

build: $(DIRS)

check:
	@echo "==> REPO: $(REPO)" ; \
	echo "==> UID: $${SUDO_UID:-$$UID}" ; \
	for d in $(DIRS) ; do \
		if [[ ! -f $$d/built ]]; then \
			$(MAKE) --silent -C $(PWD) $$d-files; \
		fi \
	done

info: $(INFO_TARGETS)

%-info:
	@cd $(PWD)/$* ; \
	makepkg --printsrcinfo | grep depends | while read p; do \
		echo "$*: $$p" ; \
	done ; \

%-chroot: chroot
	@echo "==> Setting up chroot for [$*]" ; \
	sudo rsync -a --delete -q -W -x $(CHROOTPATH64)/root/* $(CHROOTPATH64)/$* ; \

%-sync: %-chroot
	@echo "==> Syncing packages for [$*]" ; \
	if ls */*.$(PKGEXT) &> /dev/null ; then \
		sudo cp -f */*.$(PKGEXT) $(CHROOTPATH64)/$*/repo ; \
		sudo $(REPOADD) $(CHROOTPATH64)/$*/repo/$(REPO).db.tar.gz $(CHROOTPATH64)/$*/repo/*.$(PKGEXT) > /dev/null 2>&1 ; \
	fi ; \

%/built: %-sync
	@echo "==> Building [$*]" ; \
	rm -f *.log ; \
	mkdir -p $(PWD)/$*/tmp ; mv $(PWD)/$*/*$(PKGEXT) $(PWD)/$*/tmp ; \
	sudo mkdir -p $(CHROOTPATH64)/$*/build ; \
	sudo rsync -a --delete -q -W -x $(PWD)/$* $(CHROOTPATH64)/$*/build/ ; \
	_pkgrel=$$(grep '^pkgrel=' $(CHROOTPATH64)/$*/build/$*/PKGBUILD | cut -d'=' -f2 ) ;\
	_pkgrel=$$(($$_pkgrel+1)) ; \
	sed -i "s/^pkgrel=[^ ]*/pkgrel=$$_pkgrel/" $(CHROOTPATH64)/$*/build/$*/PKGBUILD ; \
	sudo systemd-nspawn -q -D $(CHROOTPATH64)/$* /bin/bash -c 'yes | $(PACMAN) -Syu && chown builduser -R /build && cd /build/$* && sudo -u builduser makepkg -L --noconfirm --holdver --nocolor -sf > makepkg.log'; \
	_pkgver=$$(bash -c "cd $(PWD)/$* ; source PKGBUILD ; if type -t pkgver | grep -q '^function$$' 2>/dev/null ; then srcdir=$$(pwd) pkgver ; fi") ; \
	if [ -z "$$_pkgver" ] ; then \
		_pkgver=$$(grep '^pkgver=' $(PWD)/$*/PKGBUILD | sed -e "s/'\|\"\|.*=//g") ; \
	fi ; \
	_pkgnames=$$(grep -Pzo "pkgname=\((?s)(.*?)\)" $(PWD)/$*/PKGBUILD | sed -e "s/\|'\|\"\|(\|)\|.*=//g") ; \
	if [ -z "$$_pkgnames" ] ; then \
		_pkgnames=$$(grep '^pkgname=' $(PWD)/$*/PKGBUILD | sed -e "s/'\|\"\|.*=//g") ; \
	fi ; \
	for pkgname in $$_pkgnames; do \
		if ! ls $(CHROOTPATH64)/$*/build/$*/$$pkgname-*$$_pkgver-$$_pkgrel-*$(PKGEXT) 1> /dev/null 2>&1; then \
			echo "==> Could not find $(CHROOTPATH64)/$*/build/$*/$$pkgname-*$$_pkgver-$$_pkgrel-*$(PKGEXT)" ; \
			rm -f $(PWD)/$*/*.$(PKGEXT) ; \
			mv $(PWD)/$*/tmp/*.$(PKGEXT) $(PWD)/$*/ && rm -rf $(PWD)/$*/tmp ; \
			exit 1; \
		else \
			cp $(CHROOTPATH64)/$*/build/$*/$$pkgname-*$$_pkgver-*$(PKGEXT) $(PWD)/$*/ ; \
		fi ; \
	done ; \
	cp $(CHROOTPATH64)/$*/build/$*/*.log $(PWD)/$*/ ; \
    cp $(CHROOTPATH64)/$*/build/$*/PKGBUILD $(PWD)/$*/PKGBUILD ; \
	rm -rf $(PWD)/$*/tmp ; \
	touch $(PWD)/$*/built

$(DIRS): chroot
	@if [ ! -f $(PWD)/$@/built ]; then \
		if ! $(MAKE) $@/built ; then \
			exit 1 ; \
		fi ; \
	fi ; \
	sudo rm -rf $(CHROOTPATH64)/$@ $(CHROOTPATH64)/$@.lock

%-deps:
	@echo "==> Marking dependencies for rebuild [$*]" ; \
	rm -f $(PWD)/$*/built ; \
	for dep in $$(grep ' $* ' $(PWD)/Makefile | cut -d':' -f1) ; do \
		$(MAKE) -s -C $(PWD) $$dep-deps ; \
	done ; \


srcpull: $(PULL_TARGETS)

%-vcs:
	@_gitroot=$$(grep '^_gitroot' $(PWD)/$*/PKGBUILD | sed -e "s/'\|\"\|.*=//g") && \
	_hgroot=$$(grep '^_hgroot' $(PWD)/$*/PKGBUILD | sed -e "s/'\|\"\|.*=//g") && \
	if [ ! -z "$$_gitroot" ] ; then \
		_gitname=$$(grep '^_gitname' $(PWD)/$*/PKGBUILD | sed -e "s/'\|\"\|.*=//g") && \
		if [ -f $(PWD)/$*/$$_gitname/HEAD ]; then \
			for f in $(PWD)/$*/*/HEAD; do \
				git --git-dir=$$(dirname $$f) remote update --prune ; \
			done ; \
		else \
			git clone --mirror $$_gitroot $(PWD)/$*/$$_gitname ; \
		fi ; \
	elif [ ! -z "$$_hgroot" ] ; then \
		_hgname=$$(grep '^_hgname' $(PWD)/$*/PKGBUILD | sed -e "s/'\|\"\|.*=//g") && \
		if [ -d $(PWD)/$*/$$_hgname/.hg ]; then \
			for f in $(PWD)/$*/*/.hg; do \
				hg --cwd=$$(dirname $$f) pull ; \
			done ; \
		else \
			 hg clone -U $$_hgroot $(PWD)/$*/$$_hgname ; \
		fi ; \
	fi ; \

%-pull: %-vcs
	@_pkgver=$$(bash -c "cd $(PWD)/$* ; source PKGBUILD ; if type -t pkgver | grep -q '^function$$' 2>/dev/null ; then pkgver ; fi") ; \
	if [ ! -z "$$_pkgver" ] ; then \
		echo "==> Updating pkgver [$*]" ; \
		sed -i "s/^pkgver=[^ ]*/pkgver=$$_pkgver/" $(PWD)/$*/PKGBUILD ; \
	else \
		_pkgver=$$(grep '^pkgver=' $(PWD)/$*/PKGBUILD | sed -e "s/'\|\"\|.*=//g") ; \
	fi ; \
	if [ ! -z "$$_pkgver" ] ; then \
		_pkgnames=$$(grep -Pzo "pkgname=\((?s)(.*?)\)" $(PWD)/$*/PKGBUILD | sed -e "s/\|'\|\"\|(\|)\|.*=//g") ; \
		if [ -z "$$_pkgnames" ] ; then \
			_pkgnames=$$(grep '^pkgname=' $(PWD)/$*/PKGBUILD | sed -e "s/'\|\"\|.*=//g") ; \
		fi ; \
		for pkgname in $$_pkgnames; do \
			if ! ls $(PWD)/$*/$$pkgname-*$$_pkgver-*$(PKGEXT) 1> /dev/null 2>&1; then \
				echo "==> Updating pkgrel [$*]" ; \
				sed -i "s/^pkgrel=[^ ]*/pkgrel=0/" $(PWD)/$*/PKGBUILD ; \
				$(MAKE) -s -C $(PWD) $*-deps ; \
				break ; \
			fi ; \
		done ; \
	fi ; \

checkvers: $(CHECKVER_TARGETS)

%-checkver:
	@_pkgver=$$(bash -c "cd $(PWD)/$* ; source PKGBUILD ; if type -t pkgver | grep -q '^function$$' 2>/dev/null ; then pkgver ; fi") ; \
	if [ ! -z "$$_pkgver" ] ; then \
		echo "==> Updating pkgver [$*]" ; \
		sed -i "s/^pkgver=[^ ]*/pkgver=$$_pkgver/" $(PWD)/$*/PKGBUILD ; \
	else \
		_pkgver=$$(grep '^pkgver=' $(PWD)/$*/PKGBUILD | sed -e "s/'\|\"\|.*=//g") ; \
	fi ; \
	echo "==> Package [$*]: $$_pkgver" ; \
	if [ ! -z "$$_pkgver" ] ; then \
		_pkgnames=$$(grep -Pzo "pkgname=\((?s)(.*?)\)" $(PWD)/$*/PKGBUILD | sed -e "s/\|'\|\"\|(\|)\|.*=//g") ; \
		if [ -z "$$_pkgnames" ] ; then \
			_pkgnames=$$(grep '^pkgname=' $(PWD)/$*/PKGBUILD | sed -e "s/'\|\"\|.*=//g") ; \
		fi ; \
		for pkgname in $$_pkgnames; do \
			if ! ls $(PWD)/$*/$$pkgname-*$$_pkgver-*$(PKGEXT) 1> /dev/null 2>&1; then \
				echo "==> Updating pkgrel [$*]" ; \
				sed -i "s/^pkgrel=[^ ]*/pkgrel=0/" $(PWD)/$*/PKGBUILD ; \
				break ; \
			fi ; \
		done ; \
	fi ; \

%-files:
	@_pkgver=$$(grep '^pkgver=' $(PWD)/$*/PKGBUILD | sed -e "s/'\|\"\|.*=//g") ; \
	_pkgrel=$$(grep '^pkgrel=' $(PWD)/$*/PKGBUILD | sed -e "s/'\|\"\|.*=//g") ; \
	_fullver="$$_pkgver-$$_pkgrel" ; \
	_pkgnames=$$(grep -Pzo "pkgname=\((?s)(.*?)\)" $(PWD)/$*/PKGBUILD | sed -e "s/\|'\|\"\|(\|)\|.*=//g") ; \
	if [ -z "$$_pkgnames" ] ; then \
		_pkgnames=$$(grep '^pkgname=' $(PWD)/$*/PKGBUILD | sed -e "s/'\|\"\|.*=//g") ; \
	fi ; \
	for _pkgname in $$_pkgnames; do \
		echo "==> Rebuild $*: $$_pkgname-$$_fullver" ; \
	done ; \

updateshas: $(SHA_TARGETS)

%-sha:
	@cd $(PWD)/$* && updpkgsums

-include Makefile.mk

adwaita-icon-theme: chroot

aisleriot: gtk3 dconf chroot

anjuta: gdl vte3 devhelp glade vala dconf gjs chroot

appstream-glib: gtk3 chroot

at-spi2-atk: at-spi2-core atk chroot

at-spi2-core: glib2 gobject-introspection gtk-doc chroot

atk: glib2 gtk-doc gobject-introspection chroot

atkmm: glibmm atk chroot

atomix: gtk3 chroot

baobab: dconf gtk3 gsettings-desktop-schemas chroot

brasero: gst-plugins-good totem-plparser gtk3 dconf libnotify gvfs chroot

california: evolution-data-server libgee chroot

cantarell-fonts: chroot

caribou: at-spi2-atk gtk3 clutter dconf vala gobject-introspection chroot

cheese: gtk3 clutter-gst gst-plugins-good gst-plugins-bad gst-plugins-base clutter-gtk gnome-video-effects gnome-desktop libgudev chroot

clutter: gtk3 gobject-introspection gtk-doc chroot

clutter-gst: clutter gst-plugins-base libgudev chroot

clutter-gtk: clutter chroot

colord: dconf libgudev chroot

dconf: glib2 vala chroot

dconf-editor: dconf gtk3 vala chroot

devhelp: webkit2gtk chroot

easytag: gtk3 chroot

empathy: clutter-gst clutter-gtk folks gcr cheese libchamplain libgudev chroot

eog: gnome-desktop libpeas dconf chroot

epiphany: libsoup libnotify gsettings-desktop-schemas webkit2gtk dconf gcr gnome-desktop libwnck3 gnome-themes-standard chroot

evince: chroot

evolution: gnome-desktop dconf evolution-data-server chroot

evolution-data-server: gnome-online-accounts libgweather libgdata chroot

evolution-ews: evolution chroot

evolution-mapi: evolution chroot

file-roller: gtk3 dconf libnotify json-glib chroot

five-or-more: gtk3 chroot

folks: libgee evolution-data-server chroot

gcr: dconf gtk3 gnome-common gobject-introspection vala chroot

geary: gnome-keyring libgee libnotify chroot

gedit: gspell gtksourceview3 gsettings-desktop-schemas libpeas dconf chroot

gegl: chroot

ghex: gtk3 chroot

gimp: gtk3 gegl mypaint libgexiv2 libgudev chroot

girl: chroot

gitg: libgit2-glib gtksourceview3 webkit2gtk libpeas chroot

gjs: gtk3 gobject-introspection chroot

gdk-pixbuf2: glib2 gtk-doc gobject-introspection chroot

gdl: gtk3 chroot

gdm: gnome-session yelp-tools gobject-introspection chroot

geocode-glib: glib2 json-glib libsoup chroot

glabels: evolution-data-server dconf chroot

glade: gtk3 chroot

glib2: chroot

glib-networking: glib2 gsettings-desktop-schemas chroot

glibmm: glib2 libsigcpp chroot

gmime: glib2 chroot

gnome-2048: clutter-gtk libgee libgames-support chroot

gnome-backgrounds: chroot

gnome-boxes: libvirt-glib clutter-gtk libsoup dconf gnome-themes-standard libgudev chroot

gnome-bluetooth: gtk3 libnotify gobject-introspection chroot

gnome-builder: gtksourceview3 devhelp gjs libpeas vte3 chroot

gnome-calculator: gtk3 dconf gtksourceview3 chroot

gnome-calendar: gtk3 evolution-data-server chroot

gnome-characters: gjs gtk3 gnome-desktop chroot

gnome-chess: gtk3 chroot

gnome-clocks: gtk3 gsound gnome-desktop libgweather chroot

gnome-color-manager: colord chroot

gnome-common: yelp-tools gtk-doc chroot

gnome-contacts: gtk3 folks gnome-desktop dconf gnome-online-accounts libgee cheese chroot

gnome-control-center: gnome-bluetooth gnome-desktop gnome-online-accounts gnome-settings-daemon gsettings-desktop-schemas gtk3 libgtop gnome-color-manager libgnomekbd cheese libgudev grilo clutter-gtk chroot

gnome-desktop: gsettings-desktop-schemas gtk3 gobject-introspection chroot

gnome-devel-docs: yelp yelp-tools chroot

gnome-dictionary: dconf gtk3 chroot

gnome-disk-utility: gtk3 libsecret chroot

gnome-documents: evince gjs gtk3 gnome-desktop gnome-online-accounts libgdata tracker webkit2gtk gnome-online-miners chroot

gnome-font-viewer: gtk3 gnome-desktop chroot

gnome-getting-started-docs: gst-plugins-base gst-plugins-good yelp chroot

gnome-initial-setup: networkmanager cheese gnome-desktop libgweather gnome-online-accounts gdm libgnomekbd chroot

gnome-keyring: gcr chroot

gnome-klotski: gtk3 libgee chroot

gnome-logs: gtk3 gsettings-desktop-schemas chroot

gnome-mahjongg: gtk3 chroot

gnome-maps: geocode-glib gjs libchamplain gnome-themes-standard libgee folks chroot

gnome-menus: glib2 gobject-introspection chroot

gnome-mines: gtk3 chroot

gnome-multi-writer: gtk3 chroot

gnome-music: grilo grilo-plugins gst-plugins-base tracker gtk3 gvfs chroot

gnome-news: chroot

gnome-nibbles: clutter-gtk chroot

gnome-online-accounts: json-glib libnotify rest libsecret gcr chroot

gnome-online-miners: libgdata gnome-online-accounts grilo chroot

gnome-packagekit: gtk3 libnotify chroot

gnome-photos: gtk3 gnome-online-accounts gnome-desktop gegl tracker libgdata gnome-online-miners chroot

gnome-robots: gtk3 glib2 libgames-support chroot

gnome-screenshot: dconf gtk3 chroot

gnome-session: dconf gsettings-desktop-schemas gtk3 gnome-desktop json-glib gtk-doc chroot

gnome-settings-daemon: dconf gnome-desktop gsettings-desktop-schemas libnotify libgweather geocode-glib networkmanager libgudev chroot

gnome-shell: caribou gcr gjs gnome-bluetooth gnome-menus gnome-session gnome-settings-daemon gnome-themes-standard gsettings-desktop-schemas libcroco gdm libsecret mutter gnome-control-center gtk-doc evolution-data-server gobject-introspection chroot

gnome-shell-extensions: gnome-shell chroot

gnome-software: gtk3 libsoup gsettings-desktop-schemas gnome-desktop chroot

gnome-sound-recorder: gtk3 gjs gst-plugins-good chroot

gnome-sudoku: dconf gtk3 libgee json-glib chroot

gnome-system-monitor: libgtop gtkmm3 chroot

gnome-taquin: gtk3 chroot

gnome-terminal: vte3 gsettings-desktop-schemas dconf chroot

gnome-tetravex: gtk3 chroot

gnome-themes-standard: cantarell-fonts gtk3 chroot

gnome-todo: libpeas evolution-data-server chroot

gnome-tweak-tool: gnome-settings-daemon chroot

gnome-user-docs: yelp chroot

gnome-user-share: dconf gtk3 libnotify chroot

gnome-video-arcade: dconf gtk3 libsoup chroot

gnome-video-effects: chroot

gnome-weather: gtk3 gjs libgweather chroot

gobject-introspection: chroot

grilo: gtk3 libsoup totem-plparser chroot

grilo-plugins: grilo chroot

gsettings-desktop-schemas: gobject-introspection chroot

gsound: glib2 chroot

gspell: gtk3 gtksourceview3 chroot

gssdp: libsoup chroot

gstreamer: glib2 chroot

gst-plugins-base: gstreamer chroot

gst-plugins-good: libsoup gst-plugins-base gdk-pixbuf2 libgudev gstreamer chroot

gst-plugins-bad: gstreamer gst-plugins-base chroot

gstreamermm: glibmm gstreamer chroot

gtksourceview3: gtk3 glade chroot

gucharmap: dconf gtk3

gupnp: gssdp chroot

gupnp-igd: gupnp chroot

gtk-doc: yelp-tools chroot

gtk3: adwaita-icon-theme json-glib rest atk at-spi2-atk gdk-pixbuf2 gobject-introspection pango chroot

gtkmm3: gtk3 pangomm atkmm mm-common chroot

gvfs: dconf libsoup libsecret libgudev gcr chroot

json-glib: glib2 gobject-introspection gtk-doc chroot

libchamplain: clutter-gtk libsoup chroot

libcroco: glib2 chroot

libgames-support: gtk3 libgee chroot

libgda: gtksourceview3 libsecret chroot

libgdata: libsoup gcr gnome-online-accounts chroot

libgee: glib2 chroot

libgexiv2: glib2 chroot

libgit2: chroot

libgit2-glib: libgit2 glib2 chroot

libgnomekbd: gtk3 dconf chroot

libgsf: gdk-pixbuf2 chroot

libgsystem: glib2 chroot

libgtop: glib2 chroot

libgudev: glib2 chroot

libgweather: libsoup gtk3 geocode-glib chroot

libgxps: chroot

libmediaart: gdk-pixbuf2 chroot

libmypaint: gobject-introspection gegl chroot

libnotify: gdk-pixbuf2 gtk3 gobject-introspection gnome-common chroot

libpeas: gtk3 gobject-introspection chroot

libsecret: glib2 gjs chroot

libsigcpp: chroot

libsoup: glib2 glib-networking chroot

libvirt-glib: chroot

libwnck3: gtk3 chroot

libzapojit: chroot

mm-common: chroot

mousetweaks: gtk3 chroot

mutter: clutter dconf gsettings-desktop-schemas zenity gnome-settings-daemon libgudev chroot

mypaint: libmypaint chroot

nautilus: gnome-desktop gvfs nautilus-sendto chroot

nautilus-sendto: glib2 chroot

networkmanager: libsoup libgudev chroot

network-manager-applet: gtk3 networkmanager chroot

openchange: chroot

pango: glib2 gobject-introspection chroot

pangomm: pango glibmm chroot

polari: gjs gtk3 chroot

pygobject: gobject-introspection chroot

rest: glib2 gobject-introspection chroot

seahorse: gtk3 gcr libsecret libsoup dconf chroot

totem: totem-plparser clutter-gtk clutter-gst gst-plugins-good gst-plugins-bad grilo dconf gnome-desktop chroot

totem-plparser: gmime libsoup chroot

tracker: gst-plugins-base libgee libsecret networkmanager gtk3 chroot

webkit2gtk: libsecret gst-plugins-base libnotify gtk3 chroot

vala: glib2 chroot

vte3: gtk3 chroot

yelp-xsl: chroot

yelp: yelp-xsl dconf chroot

yelp-tools: yelp dconf chroot

zenity: libnotify chroot
