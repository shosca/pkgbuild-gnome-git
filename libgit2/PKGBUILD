# Maintainer: Lukas Fleischer <lfleischer@archlinux.org>
# Contributor: Hilton Medeiros <medeiros.hilton@gmail.com>
# Contributor: Dave Reisner <d@falconindy.com>

_gitroot='http://github.com/libgit2/libgit2'
_gitname='repo-libgit2'
_gitbranch='maint/v0.24'

pkgname=libgit2
pkgver=0.24.0.0.r9214.g211e117
pkgrel=4
epoch=1
pkgdesc='A linkable library for Git'
arch=('i686' 'x86_64')
url='https://libgit2.github.com/'
depends=(zlib openssl libssh2 http-parser)
makedepends=(git cmake python)
license=('GPL2')
source=("$_gitname::git+$_gitroot#branch=$_gitbranch")
sha256sums=('SKIP')

pkgver() {
  cd "$_gitname"
  git symbolic-ref HEAD refs/heads/$_gitbranch
  printf "%s.%s.r%s.g%s" \
    "$(git show HEAD:include/git2/version.h | grep 'LIBGIT2_VERSION' \
      | tr -d '[]()""' | tr -s ' ' | cut -d' ' -f3)" \
    "$(git rev-list $(git rev-list --tags --no-walk --max-count=1)..HEAD --count)" \
    "$(git rev-list HEAD --count)" \
    "$(git rev-parse --short HEAD)"
}

build() {
  cd "$_gitname"
  export LANG=en_US.UTF-8
  cmake -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DTHREADSAFE:BOOL=ON

  make
}

package() {
  cd "$_gitname"
  make DESTDIR="$pkgdir" install
}
